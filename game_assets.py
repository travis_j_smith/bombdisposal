import pygame
import os

pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.init()

pygame.init()
pygame.mouse.set_visible(False)

font = pygame.font.SysFont("monospace", 20)
font.set_bold(True)
font.set_italic(True)
bomb = pygame.image.load(os.path.join("assets", "images", "bomb.png"))
explosion_image = pygame.image.load(os.path.join("assets", "images", "explosion.png"))
cursor = pygame.image.load(os.path.join("assets", "images", "cursor-pointer.png"))
countdown_font = pygame.font.Font(os.path.join("assets", "fonts", "digital-7 (mono).ttf"), 25)
code_font = pygame.font.Font(os.path.join("assets", "fonts", "digital-7 (mono).ttf"), 50)
countdown_font.set_bold(True)

beep = pygame.mixer.Sound(os.path.join("assets", "sounds", "countdown_beep.wav"))
explosion = pygame.mixer.Sound(os.path.join("assets", "sounds", "explosion.wav"))
button_click = pygame.mixer.Sound(os.path.join("assets", "sounds", "button_push.wav"))
confirm_sound = pygame.mixer.Sound(os.path.join("assets", "sounds", "confirm.wav"))
