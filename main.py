from pygame.locals import QUIT
from pygame.locals import MOUSEBUTTONDOWN
from button import Button
from game_assets import *

RED = (255, 0, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0, 0)


class Game:

    def __init__(self):
        self.screen = pygame.display.set_mode((1024, 768))
        pygame.display.set_caption('Bomb Disposal!')

        self.match_code = "90210"
        self.timer = 30
        self.code = ""

        self.diffuse_button = (0, 0, 0, 0)
        self.diffuse_colour = RED

        self.controls = [
            Button(self.screen, 375, 330, 70, 70, "1", code_font, self.button_callback),
            Button(self.screen, 460, 330, 70, 70, "2", code_font, self.button_callback),
            Button(self.screen, 545, 330, 70, 70, "3", code_font, self.button_callback),
            Button(self.screen, 375, 415, 70, 70, "4", code_font, self.button_callback),
            Button(self.screen, 460, 415, 70, 70, "5", code_font, self.button_callback),
            Button(self.screen, 545, 415, 70, 70, "6", code_font, self.button_callback),
            Button(self.screen, 375, 500, 70, 70, "7", code_font, self.button_callback),
            Button(self.screen, 460, 500, 70, 70, "8", code_font, self.button_callback),
            Button(self.screen, 545, 500, 70, 70, "9", code_font, self.button_callback),
            Button(self.screen, 375, 585, 70, 70, "C", code_font, self.clear),
            Button(self.screen, 460, 585, 70, 70, "0", code_font, self.button_callback),
            Button(self.screen, 545, 585, 70, 70, "D", code_font, self.delete),
        ]

    def button_callback(self, text):
        button_click.play(0)
        self.code += text

    def clear(self, text):
        button_click.play(0)
        self.code = ""

    def delete(self, text):
        button_click.play(0)
        self.code = self.code[:-1]

    def draw(self):
        self.screen.blit(bomb, (320, 0))
        self.diffuse_button = pygame.draw.circle(self.screen, self.diffuse_colour, (900, 650), 75)
        self.screen.blit(font.render("DIFFUSE!", True, WHITE), (845, 640))
        self.screen.blit(countdown_font.render("00:00:" + str(self.timer).zfill(2), True, RED), (428, 80))

        pygame.draw.rect(self.screen, WHITE, (250, 200, 500, 500))
        pygame.draw.rect(self.screen, BLACK, (255, 205, 490, 490))

        pygame.draw.rect(self.screen, WHITE, (350, 230, 300, 50))

        code_box = code_font.render(self.code, True, BLACK)
        code_box_size = code_box.get_rect()

        self.screen.blit(code_box, (530 - (code_box_size.width / 2), 235))

        for control in self.controls:
            control.draw()

        if self.timer <= 0:
            self.screen.blit(explosion_image, (60, -50))

        self.screen.blit(cursor, pygame.mouse.get_pos())

    def blackout(self):
        background = pygame.Surface(self.screen.get_size())
        background = background.convert()
        background.fill((0, 0, 0))
        self.screen.blit(background, (0, 0))

    def run(self):
        clock = pygame.time.Clock()
        ticks_passed = 0
        timer_running = True

        while True:
            self.blackout()
            self.draw()

            pygame.display.flip()

            if self.timer == 0:
                explosion.play(0)
                pygame.time.wait(4000)
                return

            if ticks_passed % 60 == 0 and timer_running:
                beep.play(0)
                self.timer -= 1

            for event in pygame.event.get():
                for control in self.controls:
                    try:
                        control.run_event(event)
                    except AttributeError:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    if self.diffuse_button.collidepoint(pygame.mouse.get_pos()):
                        confirm_sound.play(0)
                        self.diffuse_colour = BLUE

                        if self.code == self.match_code:
                            self.code = "Diffused!"
                            timer_running = False
                        else:
                            self.code = "OH NO!"
                            self.timer = 0
                    else:
                        self.diffuse_colour = RED
                elif event.type == QUIT:
                    return

            ticks_passed += 1
            clock.tick(60)


game = Game()
game.run()
