import pygame
from pygame.locals import MOUSEBUTTONDOWN


class Button:

    def __init__(self, screen, x, y, width, height, text, font, event=None, colour=(255, 255, 255)):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.screen = screen
        self.text = text
        self.colour = colour
        self.font = font
        self.rect = pygame.Rect(x, y, width, height)
        self.event = event

    def draw(self):
        font_box = self.font.render(self.text, True, (0, 0, 0))
        font_box_size = font_box.get_rect()

        pygame.draw.rect(self.screen, self.colour, (self.x, self.y, self.width, self.height))
        self.screen.blit(font_box, (self.x + font_box_size.width,
                                    self.y + (font_box_size.height / 2)))

    def run_event(self, event):
        if event.type == MOUSEBUTTONDOWN:
            if self.rect.collidepoint(pygame.mouse.get_pos()) and self.event:
                self.event(self.text)
